package com.example.cookdo

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter

@Suppress("DEPRECATION")
class ViewPagerAdapter(private val layouts: MutableList<Fragment>, fm: FragmentManager):
    FragmentStatePagerAdapter(fm) {
    override fun getCount() = layouts.size

    override fun getItem(position: Int): Fragment = layouts[position]
}