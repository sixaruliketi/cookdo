package com.example.cookdo.listeners

interface ClickListener {
    fun itemClickListener(position: Int)
}