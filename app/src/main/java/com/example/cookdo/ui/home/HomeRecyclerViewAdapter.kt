package com.example.cookdo.ui.home

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.cookdo.databinding.HomeItemViewBinding
import com.example.cookdo.listeners.ClickListener

class HomeRecyclerViewAdapter (private val items: MutableList<Drinks>, private val clickListener: ClickListener):
    RecyclerView.Adapter<HomeRecyclerViewAdapter.ItemViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val itemView =
            HomeItemViewBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ItemViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        holder.bind()
    }

    override fun getItemCount() = items.size

    inner class ItemViewHolder(private val binding: HomeItemViewBinding):
        RecyclerView.ViewHolder(binding.root), View.OnClickListener {
            private lateinit var model: Drinks
            fun bind() {
                model = items[adapterPosition]
                binding.postIcon.setImageResource(model.imageRecourse)
                binding.postIngredientOne.text = model.ingredient1
                binding.postIngredientTwo.text = model.ingredient2
                binding.postIngredientThree.text = model.ingredient3
                binding.root.setOnClickListener(this)
            }

        override fun onClick(v: View?) {
            clickListener.itemClickListener(adapterPosition)
        }
    }
}