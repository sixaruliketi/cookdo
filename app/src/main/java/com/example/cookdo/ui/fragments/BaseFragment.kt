package com.example.cookdo.ui.fragments

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.viewbinding.ViewBinding
import com.example.cookdo.MainActivity
import com.example.cookdo.ui.vm.HomeViewModel

typealias Inflate<T> = (LayoutInflater, ViewGroup?, Boolean) -> T

abstract class BaseFragment<VB: ViewBinding, VM: ViewModel>(private val inflate: Inflate<VB>, viewModel: Class<VM>): Fragment() {

    fun getViewModelClass() = HomeViewModel::class.java

    val viewModel : VM by lazy {
        ViewModelProvider(requireActivity()).get(viewModel)
    }

    var binding: VB? = null

    var mainActivity: MainActivity? = null

    override fun onAttach(context: Context) {
        if (context is MainActivity)
            mainActivity = context
        super.onAttach(context)
    }

    override fun onDetach() {
        mainActivity = null
        super.onDetach()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (binding == null) {
            binding = inflate(inflater,container, false)
                start(inflater,container)
        }

        return binding!!.root
    }

    abstract fun start(inflater: LayoutInflater, container: ViewGroup?)

}