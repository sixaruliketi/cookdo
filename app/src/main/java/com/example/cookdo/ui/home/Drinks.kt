package com.example.cookdo.ui.home

data class Drinks (val title: String? = null,
                   val ingredient1: String,
                   val ingredient2: String,
                   val ingredient3: String,
                   val imageRecourse :Int)