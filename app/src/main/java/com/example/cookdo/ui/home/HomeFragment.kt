package com.example.cookdo.ui.home

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.navigation.findNavController
import com.example.cookdo.R
import com.example.cookdo.databinding.FragmentHomeBinding
import com.example.cookdo.listeners.ClickListener
import com.example.cookdo.ui.fragments.BaseFragment
import com.example.cookdo.ui.vm.HomeViewModel

class HomeFragment : BaseFragment<FragmentHomeBinding, HomeViewModel>(FragmentHomeBinding::inflate, HomeViewModel::class.java) {

    override fun start(inflater: LayoutInflater, container: ViewGroup?) {
        viewModel.init()
        init()
    }

    private lateinit var adapter: HomeRecyclerViewAdapter

    private fun init() {
        adapter = HomeRecyclerViewAdapter(mutableListOf(), object : ClickListener {
            override fun itemClickListener(position: Int) {
                view!!.findNavController().navigate(R.id.action_navigation_home_to_postFragment)
            }
        })
    }
}