package com.example.cookdo.ui.explore

data class ExploreItem(val ExploreItemBg: Int,
                       val ExploreItemIcon: Int,
                       val ExploreItemTitle: String
)
