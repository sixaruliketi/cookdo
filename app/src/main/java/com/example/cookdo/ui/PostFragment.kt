package com.example.cookdo.ui

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.example.cookdo.R
import com.example.cookdo.databinding.FragmentPostBinding

class PostFragment : Fragment() {

    private lateinit var binding: FragmentPostBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentPostBinding.inflate(inflater,container,false)
        init()
        return binding.root
    }

    private fun init() {
        binding.backButton.setOnClickListener {
            findNavController().navigate(R.id.action_postFragment_to_navigation_home)
        }
    }
}