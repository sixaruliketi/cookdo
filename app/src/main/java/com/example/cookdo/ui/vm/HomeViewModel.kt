package com.example.cookdo.ui.vm

import androidx.lifecycle.ViewModel
import com.example.cookdo.R
import com.example.cookdo.ui.home.Drinks

class HomeViewModel: ViewModel() {

    private val items = mutableListOf<Drinks>()


    fun init(){
        setData()
    }

    private fun setData() {
        items.add(
            Drinks(
                "Classic B-52",
                "1/3 ounce coffee liqueur",
                "1/3 ounce Baileys Irish cream liqueur",
                "1/3 ounce Grand Marnier liqueur",
                R.mipmap.b52
            )
        )
        items.add(
            Drinks(
                "Irish Bomb",
                "1/2 ounce Irish whiskey",
                "1/2 ounce Baileys Irish cream",
                "Guinness beer",
                R.mipmap.irish_bomb
            )
        )
        items.add(
            Drinks(
                "White Russian",
                "2 ounces vodka",
                "1 ounce Kahlúa",
                "1 splash heavy cream",
                R.mipmap.white_russian
            )
        )
        items.add(
            Drinks(
                "Pink Grapefruit Margaritas",
                "8 ruby red grapefruits",
                "6 fl oz silver tequila (170 mL)",
                "sugar, for rim",
                R.mipmap.pink_grapefruit_margaritas
            )
        )
        items.add(
            Drinks(
                "Grasshopper",
                "1 ounce green crème de menthe",
                "1 ounce white crème de cacao",
                "2 ounces heavy cream",
                R.mipmap.grasshopper
            )
        )
        items.add(
            Drinks(
                "Parisian Blonde",
                "1 ounce Jamaican rum",
                "1 ounce orange curaçao",
                "1 ounce heavy cream",
                R.mipmap.parisian_blonde
            )
        )
        items.add(
            Drinks(
                "French 75",
                "1/2 ounce lemon juice, freshly squeezed",
                "3 ounces Champagne",
                "1 ounce gin",
                R.mipmap.french75
            )
        )
        items.add(
            Drinks(
                "White Lady",
                "2 ounces gin",
                "1/2 ounce orange liqueur or triple sec",
                "1/2 ounce lemon juice, freshly squeezed",
                R.mipmap.white_lady
            )
        )
        items.add(
            Drinks(
                "Daiquiri",
                "2 ounces light rum",
                "1 ounce lime juice, freshly squeezed",
                "3/4 ounce demerara sugar syrup",
                R.mipmap.daiquiri
            )
        )
        items.add(
            Drinks(
                "CBD Lavender Lemon",
                "sprigs fresh rosemary",
                "oz gin (120 mL)",
                "teaspoons CBD oil",
                R.mipmap.cbd_lavender_lemon
            )
        )
        items.add(
            Drinks(
                "Watermelon Cucumber Cooler",
                "oz gin (60 mL)",
                "½ oz lime juice (15 mL)",
                "1 ½ oz watermelon (45 mL), juiced",
                R.mipmap.watermelon_cucumber_cooler
            )
        )
        items.add(
            Drinks(
                "Papa Smurf",
                "½ oz sloeberry vodka (15 mL)",
                "½ oz rich simple syrup (15 g)",
                "1 oz blue curaçao (1 mL)",
                R.mipmap.papa_smurf
            )
        )
    }
}