package com.example.cookdo.ui.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import com.example.cookdo.R
import com.example.cookdo.databinding.FragmentExploreBinding
import com.example.cookdo.listeners.ClickListener
import com.example.cookdo.ui.explore.ExploreItem
import com.example.cookdo.ui.explore.ExploreRecyclerViewAdapter

class ExploreFragment : Fragment() {

    private lateinit var binding: FragmentExploreBinding
    private lateinit var adapter : ExploreRecyclerViewAdapter
    private var items = mutableListOf<ExploreItem>()


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentExploreBinding.inflate(inflater, container, false)
        init()

        return binding.root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = FragmentExploreBinding.inflate(layoutInflater)
    }

    private fun init(){
        setData()
        adapter = ExploreRecyclerViewAdapter(items, object : ClickListener{
            override fun itemClickListener(position: Int) {
                 view!!.findNavController().navigate(R.id.action_navigation_explore_to_postFragment)
            }

        })
        binding.ExploreRecyclerView.layoutManager = GridLayoutManager(context, 2)
        binding.ExploreRecyclerView.adapter = adapter
    }

    private fun setData() {
        items.add(ExploreItem(R.drawable.explore_item_bg, R.drawable.ic_baseline_star_border_24,"explore item"))
        items.add(ExploreItem(R.drawable.explore_item_bg, R.drawable.ic_baseline_star_border_24,"explore item"))
        items.add(ExploreItem(R.drawable.explore_item_bg, R.drawable.ic_baseline_star_border_24,"explore item"))
        items.add(ExploreItem(R.drawable.explore_item_bg, R.drawable.ic_baseline_star_border_24,"explore item"))
        items.add(ExploreItem(R.drawable.explore_item_bg, R.drawable.ic_baseline_star_border_24,"explore item"))
        items.add(ExploreItem(R.drawable.explore_item_bg, R.drawable.ic_baseline_star_border_24,"explore item"))
    }
}
