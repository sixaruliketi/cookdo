package com.example.cookdo.ui.explore

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.cookdo.databinding.ExploreItemViewBinding
import com.example.cookdo.listeners.ClickListener

class ExploreRecyclerViewAdapter(private val items: MutableList<ExploreItem>, private val clickListener: ClickListener):
    RecyclerView.Adapter<ExploreRecyclerViewAdapter.ItemViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val itemView =
            ExploreItemViewBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ItemViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        holder.bind()
    }

    override fun getItemCount() = items.size

    inner class ItemViewHolder(private val binding: ExploreItemViewBinding):
        RecyclerView.ViewHolder(binding.root), View.OnClickListener {
        private lateinit var model: ExploreItem
        fun bind(){
            model = items[adapterPosition]
            binding.ivExploreItembg.setImageResource(model.ExploreItemBg)
            binding.ivExploreItemIcon.setImageResource(model.ExploreItemIcon)
            binding.tvExploreItemTitle.text = model.ExploreItemTitle
            binding.root.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
            clickListener.itemClickListener(adapterPosition)
        }
    }
}