package com.example.cookdo

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.viewpager.widget.ViewPager
import com.example.cookdo.databinding.ActivityMainBinding
import com.example.cookdo.ui.fragments.ExploreFragment
import com.example.cookdo.ui.home.HomeFragment

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        init()
    }

    private fun init(){
        val fragments = mutableListOf<Fragment>()
        fragments.add(HomeFragment())
        fragments.add(ExploreFragment())
        binding.viewPager.adapter = ViewPagerAdapter(fragments, supportFragmentManager)

        binding.viewPager.addOnPageChangeListener(object: ViewPager.OnPageChangeListener{
            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {
            }

            override fun onPageSelected(position: Int) {
                selectMenuItem(position)
            }

            override fun onPageScrollStateChanged(state: Int) {
            }
        })

        binding.navView.setOnNavigationItemSelectedListener {
            when(it.itemId){
                R.id.navigation_home -> binding.viewPager.currentItem = 0
                R.id.navigation_explore -> binding.viewPager.currentItem = 1
            }
            true
        }
    }
    private fun selectMenuItem(position: Int) {
        binding.navView.menu.getItem(position).isChecked = true
    }

}